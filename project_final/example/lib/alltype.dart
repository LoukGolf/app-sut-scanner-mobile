import 'package:flutter/material.dart';
import 'package:project_final/attendence1.dart';
import 'package:project_final/home.dart';
import 'package:project_final/series.dart';

class AlltypePage extends StatefulWidget {
  AlltypePage({Key key, this.subjectName,this.subjectId,this.subjectCode,this.name}) : super (key : key);
  final String subjectName,subjectId,name,subjectCode;

  @override
  _AlltypePageState createState() => _AlltypePageState();
}

class _AlltypePageState extends State<AlltypePage> {

  createAlertDialog1(BuildContext context){
    TextEditingController scoreController = new TextEditingController();
    return showDialog(context: context,builder: (context){
      return AlertDialog(
        title: Text("จัดการคะแนน"),
        content: TextField(
          controller: scoreController,
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              MaterialButton(
                elevation: 5.0,
                child: Text('Save'),
                onPressed: () {
                  var optionRounte = new MaterialPageRoute(
                    builder: (BuildContext context) => Attendence1(
                      // score: scoreController.text,
                      // name: widget.name,
                    ),
                  );
                  Navigator.of(context).pushReplacement(optionRounte);
                },
              ),
              SizedBox(width: 10),
              MaterialButton(
                elevation: 5.0,
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          )
        ],
      );
    });
  }

createAlertDialog(BuildContext context){
    TextEditingController scoreController = new TextEditingController();
    return showDialog(context: context,builder: (context){
      return AlertDialog(
        title: Text("จัดการคะแนน"),
        content: TextField(
          controller: scoreController,
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              MaterialButton(
                elevation: 5.0,
                child: Text('Save'),
                onPressed: () {
                  var optionRounte = new MaterialPageRoute(
                    // builder: (BuildContext context) => HomePage(
                    //   score: scoreController.text,
                    //   name: widget.name,
                    // ),
                  );
                  Navigator.of(context).pushReplacement(optionRounte);
                },
              ),
              SizedBox(width: 10),
              MaterialButton(
                elevation: 5.0,
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          )
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('${widget.name}',textAlign: TextAlign.center),
        backgroundColor: Colors.red,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            new ListTile(
              title: Text('${widget.subjectId}',style: TextStyle(fontSize: 30.0),textAlign: TextAlign.center),
            ),
            new ListTile(
              title: Text('${widget.subjectName}',style: TextStyle(fontSize: 10.0)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new RaisedButton(
                  child: Text('สแกนเป็นชุด',style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  //onPressed: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => Scanner()));},
                  onPressed: () {
                    createAlertDialog1(context);
                  },
                ),
                SizedBox(width: 12),
                new RaisedButton(
                  child: Text('สแกนรายบุคคล',style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  //onPressed: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => Scanner()));},
                  onPressed: () {
                    createAlertDialog(context);
                  },
                ),
              ],
            ),
            new Divider(color: Colors.black),
            new ListTile(
              title: new Text('${widget.name}',textAlign: TextAlign.center,),
            ),
            new Divider(color: Colors.black,),
          ],
        ),  
      ),
    );
  }
}