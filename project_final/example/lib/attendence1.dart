import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:project_final/attendence.dart';
class Attendence1 extends StatefulWidget {
  Attendence1({Key key, this.subjectName,this.subjectId,this.subjectCode}) : super (key : key);
  final String subjectName,subjectId,subjectCode;

  @override
  _Attendence1State createState() => _Attendence1State();
}

class _Attendence1State extends State<Attendence1> {
  Future<String> _getcolumn() async{
    var data = await http.get("https://sutclass.herokuapp.com/QColumn/"+widget.subjectCode+"/Attendence");
    var jsonData = json.decode(data.body);

    var optionRounte = new MaterialPageRoute(
      builder: (BuildContext context) => AttendencePage(
        subjectId: widget.subjectId,
        subjectName: widget.subjectName,
        subjectCode: widget.subjectCode,
        qColumn: jsonData.toString(),                
      ),
    );
    Navigator.of(context).push(optionRounte);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Attendance',textAlign: TextAlign.center),
        backgroundColor: Colors.red,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            new ListTile(
              title: Text('${widget.subjectId}',style: TextStyle(fontSize: 30.0),textAlign: TextAlign.center),
            ),
            new ListTile(
              title: Text('${widget.subjectName}',style: TextStyle(fontSize: 10.0)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new RaisedButton(
                  child: Text('NEW ATTENDANCE',style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  //onPressed: () {Navigator.push(context, new MaterialPageRoute(builder: (context) => AttendencePage()));},
                  onPressed: () => QColumns(),color: Colors.white
                                  ),
                                ],
                              ),
                              new Divider(color: Colors.black),
                              new ListTile(
                                title: new Text('Attendance',textAlign: TextAlign.center,),
                              ),
                              
                              new Divider(color: Colors.black,),
                            ],
                          ),  
                        ),
                      );
                    }
                  
                    QColumns() async{
                      _getcolumn();
                    }
}
