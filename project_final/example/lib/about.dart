import 'package:flutter/material.dart';

import 'home.dart';
import 'login.dart';

class AboutPage extends StatefulWidget {
  AboutPage({Key key, this.username, this.email, this.tcodes}) : super (key : key);
  final String username,email,tcodes;

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  createAlertDialog(BuildContext context){
    TextEditingController scoreController = new TextEditingController();
    return showDialog(context: context,builder: (context){
      return AlertDialog(
        title: Text("Your Name"),
        content: TextField(
          controller: scoreController,
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text('OK'),
            onPressed: () {},
          )
        ],
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('About as',textAlign: TextAlign.center),
        backgroundColor: Colors.red,
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("${widget.username}"),
              accountEmail: new Text("${widget.email}"),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.red,
                child: new Text("P" ,style: TextStyle(fontSize: 10.0),),
              ),

            ),
            new ListTile(
              title: new Text("Home"),
              trailing: new Icon(Icons.home),
              onTap: () {
                var homeRoute = new MaterialPageRoute(
                  builder: (BuildContext context) => HomePage(
                    username: widget.username,
                    email: widget.email,
                    tcodes: widget.tcodes,
                  ),
                );
                Navigator.of(context).pushReplacement(homeRoute);
              },
            ),
            new ListTile(
              title: new Text("About us"),
              trailing: new Icon(Icons.account_box),
              onTap: () {
                Navigator.of(context).pop();
              }
            ),
            new Divider(),
            new ListTile(
              title: new Text("Sign Out"),
              trailing: new Icon(Icons.close),
              onTap: (){Navigator.push(context, new MaterialPageRoute(builder: (context) => LoginPage()));},
            )
          ],
        ),
      ),
      body: Builder(builder: (context){
        return Container(
          child: Center(
            child: FlatButton(
              padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)
              ),
              onPressed: () {
                createAlertDialog(context);
              },
              child: Text(
                'OK',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
          ),
        );
      })
    );
  }
}