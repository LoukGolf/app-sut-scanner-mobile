// import 'package:flutter/material.dart';
// import 'package:barcode_scan/barcode_scan.dart';
// import 'package:flutter/services.dart';

// class SeriesPage extends StatefulWidget {
//   SeriesPage({Key key, this.score,this.name}) : super (key : key);
//   final String score,name;
//   @override
//   _SeriesPageState createState() => _SeriesPageState();
// }

// class _SeriesPageState extends State<SeriesPage> {
//   String result='';
//   Future _scanQR() async{
//     try{
//       String qrResult = await BarcodeScanner.scan();
//       setState(() {
//         result = qrResult;
//       });
//     } on PlatformException catch(ex){
//       if(ex.code == BarcodeScanner.CameraAccessDenied){
//         setState(() {
//           result = "Camera permission was denide";
//         });
//       }else{
//         setState(() {
//           result = "Unknown Error $ex";
//         });
//       }
//     } on FormatException{
//       setState(() {
//         result = "You pressed the back button before scanning anything"; 
//       });
//     }catch(ex){
//       setState(() {
//           result = "Unknown Error $ex";
//         });
//     }

//   }
//   @override
//   Widget build(BuildContext context) {
//     int scanS = int.parse(widget.score);
//     return Scaffold(
//       appBar: new AppBar(
//         title: new Text('${widget.name}',textAlign: TextAlign.center),
//         backgroundColor: Colors.red,
//       ),
//       body: Container(
//         child: FutureBuilder(
//           builder: (BuildContext context, AsyncSnapshot snapshot){
//             return ListView.builder(
//               itemCount: scanS,
//               itemBuilder: (context,int index){
//                 return Column(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     new ListTile(
//                       title: Text('${scanS-index}',style: TextStyle(fontSize: 18.0),textAlign: TextAlign.center),
//                       onTap: () {
//                         _scanQR();
//                       },
//                     ),
//                     new Divider(color: Colors.black),
//                   ],
//                 );
//               },
//             );
//           }
//         ),
//       ),
//       // floatingActionButton: FloatingActionButton.extended(
//       //   icon: Icon(Icons.camera_alt),
//       //   label: Text("Scan"),
//       //   onPressed: _scanQR,
//       // ),
//       // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//     );
//   }
// }