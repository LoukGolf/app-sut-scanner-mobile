// import 'package:flutter/material.dart';
// import 'dart:async';
// import 'package:http/http.dart' as http;
// import 'dart:convert';


// class Score extends StatefulWidget {
//   Score({Key key, this.title}) : super(key: key);

//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<Score> {
  
//   Future<List<Student>> _getStudents() async{

//     var data = await http.get("https://demosutclassroom.herokuapp.com/QTypename/523495_3-61");
//     var jsonData = json.decode(data.body);
//     List<Student> students = [];

//     for(var i in jsonData){
//       Student student = Student(i["typeId"], i["typeName"], i["typePercent"], i["typeNum"]);
//       students.add(student);
//     }
    
//     return students;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: new AppBar(
//         title: new Text('Score'),
//         backgroundColor: Colors.red,

//       ),
//       body: Container(
//         child: FutureBuilder(
//           future: _getStudents(),
//           builder: (BuildContext context, AsyncSnapshot snapshot) {
//             if (snapshot.data == null) {
//               return Container(
//                 child: Center(
//                   child: Text("Loading..."),
//                 ),
//               );
//             } else {
//               return ListView.builder(
//                 itemCount: snapshot.data.length,
//                 itemBuilder: (BuildContext context, int typeId) {
//                   return ListTile(
//                     title: Text(snapshot.data[typeId].typeName),
//                     subtitle: Text(snapshot.data[typeId].typePercent.toString()),
//                     onTap: (){
//                       Navigator.push(context, new MaterialPageRoute(builder: (context) => DetaiPage(snapshot.data[typeId])));
//                     },
//                   );
//                 },
//               );
//             }
//           },
//         ),
//       ),
//     );
//   }
// }

// class DetaiPage extends StatelessWidget {

//   final Student student;
//   DetaiPage(this.student);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(student.typeName),
//         backgroundColor: Colors.red,
//       ),
//       body: Stack(
//         fit: StackFit.expand,
//         children: <Widget>[
//           Column(children: <Widget>[
//             SizedBox(height: 10.0,),
//             RaisedButton(onPressed: () {Navigator.push(context,new MaterialPageRoute(builder: (context) => Scanner()));},child: Text('Scan',),)
//           ],)
//         ],
//       ),
//     );
//   }
// }
// class Student {
//   final int typeId;
//   final String typeName ;
//   final int typePercent ;
//   final int typeNum ;


//   Student(this.typeId, this.typeName, this.typePercent, this.typeNum);
// }




